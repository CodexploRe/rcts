## Module Rcts

该模块包含了个人学习go语言过程中开发的各种包，作用各不相同

- proxy

  > 获取有效的代理ip

- ...(to be continued)

若需使用，请在构建好的项目文件目录中使用以下命令安装此包

```cmd
go get gitee.com/CodexploRe/rcts
```

之后在有需要的go文件中使用import关键字导入需要的包使用即可

```go
import (
	"gitee.com/CodexploRe/rcts/ThePackageYouNeed"
)
```

